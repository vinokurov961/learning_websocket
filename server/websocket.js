const ws = require("ws");

const wss = new ws.Server(
  {
    port: 5000,
  },
  () => console.log(`Server start om port 5000`)
);

wss.on("connection", function connection(ws) {
  // ws.id = Date.now()
  ws.on("message", function (message) {
    message = JSON.parse(message);
    switch (message.event) {
      case "message":
        broadcaseMessage(message);
        break;
      case "connection":
        broadcaseMessage(message);
        break;
    }
  });
});

// function broadcaseMessage(message, id) {
function broadcaseMessage(message) {
  wss.clients.forEach((client) => {
    // if (client.id === id ) {
    client.send(JSON.stringify(message));
    // }
  });
}
