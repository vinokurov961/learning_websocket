import React, { useRef, useState } from "react";
import axios from "axios";

const WebSock = () => {
  const [messages, setMessages] = useState([]);
  const [value, setValue] = useState("");
  const socket = useRef();
  const [connected, setConnected] = useState(false);
  const [username, setUsername] = useState("");

  const sendMessage = async () => {
    const message = {
      username,
      message : value,
      id: Date.now(),
      event: 'message'
    };
    socket.current.send(JSON.stringify(message))
    setValue('')
    // await axios.post("http://localhost:5000/new-messages", {
    //   message: value,
    //   id: Date.now(),
    // });
  };

  function connect() {
    socket.current = new WebSocket("ws://localhost:5000");
    socket.current.onopen = () => {
      setConnected(true);
      const message = {
        event: 'connection',
        username,
        id: Date.now()
      }
      socket.current.send(JSON.stringify(message))
      console.log('Подключение установлено');
    };
    socket.current.onmessage = (event) => {
      const message = JSON.parse(event.data)
      setMessages(prev => [message, ...prev])
    };
    socket.current.onclose = () => {
      console.log("Socket закрыт");
    };
    socket.current.onerror = () => {
      console.log("Socket произошла ошибка");
    };
  }

  if (!connected) {
    return (
      <div className="center">
        <div>
          <div className="form">
            <input
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              placeholder="Введите ваше имя"
              type="text"
            />
            <button onClick={connect}>Войти</button>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="center">
      <div>
        <div className="form">
          <input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            type="text"
          />
          <button onClick={sendMessage}>Отправить</button>
        </div>
        <div className="messages">
          {messages.map((mess) => (
            <div key={mess.id} className="message">
              {mess.event === 'connection' ?
                <div>Пользователь {mess.username} подключился</div> : <div>Пользователь {mess.username}. {mess.message}</div>}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default WebSock;
