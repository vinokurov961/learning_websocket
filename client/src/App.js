import './App.css';
import EventSourcing from './EventSourcing';
import LognPulling from './LognPulling';
import WebSock from './WebSocket';

const App = () => {
  return (
    <div>
      {/* <LognPulling /> */}
      {/* <EventSourcing /> */}
      <WebSock />
    </div>
  );
}

export default App;
